<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 	 prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:choose>
	<c:when test="${mode == 'insert'}">
		<s:url value="/citta/insert" var="action"></s:url>
	</c:when>
	<c:when test="${mode == 'update'}">
		<s:url value="/citta/update" var="action"></s:url>
	</c:when>
</c:choose>


	
	<form action="${action}" method="post">
		<s:message code="citta.descrizione"/>:<input type="text" name="descrizione" value="${citta.descrizione}"><br/>
		<s:message code="citta.provincia"/>:
		<select name="provincia.idProvincia">
			<option></option>
			<c:forEach items="${listaProvince}" var="provincia">
				<option value="${provincia.idProvincia}">${provincia.descrizione}</option>
			</c:forEach>
		</select>
		<input type="hidden" name="idCitta" value="${citta.idCitta}"><br/>
		
		<input type="submit" value="<s:message code="button.salva"/>">			
	</form>		
</body>
</html>