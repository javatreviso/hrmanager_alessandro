<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<s:url value="/citta/insertForm" var="insert"></s:url>
<s:url value="/citta/updateForm" var="update"></s:url>
<s:url value="/citta/delete" var="delete"></s:url>

<body>

	${messaggio}

	<table>
		<tr>
			<th><s:message code="citta.descrizione"/></th>
		

			<th><a href="${insert}">
					<s:message code="button.inserisci"/>
				</a>
			</th>
		</tr>
		<c:forEach items="${listaCitta}" var="citta">
			<tr>
				<td>${citta.descrizione}</td>
				<td>
					<a href="${update}/${citta.idCitta}"><s:message code="button.modifica"/></a>
					<a href="${delete}/${citta.idCitta}"><s:message code="button.elimina"/></a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>