<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<html>
<head>

<title>HR Manager</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/css/custom.css"></s:url>">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

</head>

<body class="h-100 w-100"
	style="background-repeat: no-repeat;
							    background-position: center center;
							    background-size: cover;
							    background-image: url('<s:url value="/images/background_login.jpeg"></s:url>');
							    ">
	<div class="container-fluid h-100">

		<div class="row h-100">

			<div class="m-auto bg-white col-sm-12 col-md-4 rounded shadow px-4 pt-4 pb-1"
				style="opacity: 0.9;">
				<c:if test="${not empty errorMessge}">
					<div>${errorMessge}</div>
				</c:if>

				<form name='login' action='<s:url value="/login"/>'
					method='POST'>
					<div class="form-row pb-4">
						<div class="form-group col-6 text-left">
							<h4>Hr Manager</h4>
						</div>
						<div class="form-group col-6 text-right">
							<img src="https://www.betacom.it/wp-content/uploads/2018/10/logo_betacom-2.png"
								 height="30" 
								 alt="">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
							<label for="username"><s:message code="utenze.username" /></label>
							<input type="text" class="form-control" name="username" id="username" value=""
								placeholder="Username" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
						<label for="username"><s:message code="utenze.password" /></label>
							<input type="password" class="form-control" name="password" id="password"
								value="" placeholder="Password" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12 text-right">
							<button class="btn btn-secondary" type="submit" name="submit" value="submit">Login</button>
						</div>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>
