<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<s:url value="/province/insertForm" var="insert"></s:url>
<s:url value="/province/updateForm" var="update"></s:url>
<s:url value="/province/delete" var="delete"></s:url>

<body>

	${messaggio}

	<table>
		<tr>
			<th><s:message code="province.stato"/></th>
			<th><s:message code="province.descrizione"/></th>
			<th><s:message code="province.citta"/></th>
			<th><a href="${insert}">
					<s:message code="button.inserisci"/>
				</a>
			</th>
		</tr>
		<c:forEach items="${listaProvince}" var="province">
			<tr>
				<td>${province.stato.descrizione}</td>
				<td>${province.descrizione}</td>
				<td>
					<ul>
						<c:forEach items="${province.listaCitta}" var="citta">
							<li>${citta.descrizione}</li>
						</c:forEach>
					</ul>
				</td>
				<td>
					<a href="${update}/${province.idProvincia}"><s:message code="button.modifica"/></a>
					<a href="${delete}/${province.idProvincia}"><s:message code="button.elimina"/></a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>