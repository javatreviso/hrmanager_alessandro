<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 	 prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:choose>
	<c:when test="${mode == 'insert'}">
		<s:url value="/province/insert" var="action"></s:url>
	</c:when>
	<c:when test="${mode == 'update'}">
		<s:url value="/province/update" var="action"></s:url>
	</c:when>
</c:choose>


	
	<form action="${action}" method="post">
		<s:message code="province.descrizione"/>:<input type="text" name="descrizione" value="${province.descrizione}"><br/>
		<s:message code="province.stato"/>:
		<select name="stato.idStato">
			<option></option>
			<c:forEach items="${listaStati}" var="stato">
				<option value="${stato.idStato}">${stato.descrizione}</option>
			</c:forEach>
		</select>
		<input type="hidden" name="idProvincia" value="${province.idProvincia}"><br/>
		
		<input type="submit" value="<s:message code="button.salva"/>">			
	</form>		
</body>
</html>