<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HrManager</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<s:url value="/css/custom.css"></s:url>">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

</head>

<c:choose>
	<c:when test="${mode == 'insert'}">
		<s:url value="/anagrafica/insert" var="action"></s:url>
	</c:when>
	<c:when test="${mode == 'update'}">
		<s:url value="/anagrafica/update" var="action"></s:url>
	</c:when>
</c:choose>

<body class="bg-white">

	<nav
		class="navbar navbar-expand-lg navbar-light shadow-sm bg-white fixed-top"
		style="opacity: 0.8"
		>
		<a class="navbar-brand" href="#"> <img
			src="https://www.betacom.it/wp-content/uploads/2018/10/logo_betacom-2.png"
			height="30" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarText" aria-controls="navbarText"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link"	href="<s:url value="/homepage"/>">
					<s:message code="menu.home" /></a>
				</li>
				<li class="nav-item active"><a class="nav-link"
					href="<s:url value="/anagrafica"/>"><s:message
							code="menu.anagrafica" /></a></li>
				<li class="nav-item"><a class="nav-link"
					href="<s:url value="/utenze"/>"><s:message code="menu.utenze" /></a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <s:message code="menu.system" />
				</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<s:url value="/stati"/>"><s:message
								code="menu.system.stati" /></a> <a class="dropdown-item"
							href="<s:url value="/province"/>"><s:message
								code="menu.system.province" /></a> <a class="dropdown-item"
							href="<s:url value="/citta"/>"><s:message
								code="menu.system.citta" /></a>
					</div></li>
			</ul>
			<a class="navbar-text nav-link" href="/logout"> Logout </a>
		</div>
	</nav>

	
	<div class="jumbotron jumbotron-fluid text-white"
		style="background-image: url('https://www.strath.ac.uk/media/1newwebsite/departmentsubject/business/Business_people_in_office.jpg'); background-position: center top;">
		<div class="container mb-4 pt-5 text-center" style="text-shadow: 1px 1px 2px #000000;">

			<c:choose>
				<c:when test="${mode == 'insert'}">
					<h1 class="display-4 font-weight-bold">
						<s:message code="anagrafica.title.insert" />
					</h1>
				</c:when>
				<c:when test="${mode == 'update'}">
					<h1 class="display-4 font-weight-bold">
						<s:message code="anagrafica.title.update" />
					</h1>
				</c:when>
			</c:choose>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 bg-white mt-4 mb-4 pt-4 rounded border shadow">
				<form action="${action}" method="post">
					<div class="form-row">
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="cognome"><s:message code="anagrafica.cognome" /></label>
							<input type="text" class="form-control" name="cognome"
								id="cognome" value="${anagrafica.cognome}" required>
						</div>
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="nome"><s:message code="anagrafica.nome" /></label> <input
								type="text" class="form-control" name="nome" id="nome"
								value="${anagrafica.nome}" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="dataNascita"><s:message
									code="anagrafica.dataNascita" /></label> <input type="text"
								class="form-control" id="dataNascita" name="dataNascitaString"
								value="${anagrafica.dataNascitaString}" required>
						</div>
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="sesso"><s:message code="anagrafica.sesso" /></label>
							<select name="sesso" class="form-control" id="sesso" required>
								<option value=""></option>
								<option value="M" ${anagrafica.sesso=='M'?'selected':''}><s:message
										code="anagrafica.sesso.maschio" /></option>
								<option value="F" ${anagrafica.sesso=='F'?'selected':''}><s:message
										code="anagrafica.sesso.femmina" /></option>
								<option value="X" ${anagrafica.sesso=='X'?'selected':''}><s:message
										code="anagrafica.sesso.altro" /></option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="telefono"><s:message
									code="anagrafica.telefono" /></label> <input type="tel"
								class="form-control" name="telefono" id="telefono"
								value="${anagrafica.telefono}" required>
						</div>
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="email"><s:message code="anagrafica.mail" /></label>
							<input type="email" class="form-control" name="mail" id="email"
								value="${anagrafica.mail}" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="statoCivile"><s:message
									code="anagrafica.statoCivile" /></label> 
							<select name="statoCivile"
								class="form-control" id="statoCivile" required>
								<option value=""></option>
								<option value="celibe"
									${anagrafica.statoCivile=='celibe'?'selected':''}><s:message
										code="anagrafica.statocivile.celibe" /></option>
								<option value="sposato"
									${anagrafica.statoCivile=='sposato'?'selected':''}><s:message
										code="anagrafica.statocivile.sposato" /></option>
								<option value="vedovo"
									${anagrafica.statoCivile=='vedovo'?'selected':''}><s:message
										code="anagrafica.statocivile.vedovo" /></option>
								<option value="altro"
									${anagrafica.statoCivile=='altro'?'selected':''}><s:message
										code="anagrafica.statocivile.altro" /></option>
							</select>
						</div>
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="codiceFiscale"><s:message
									code="anagrafica.codiceFiscale" /></label> <input type="text"
								class="form-control" name="codiceFiscale" id="codiceFiscale"
								value="${anagrafica.codFiscale}" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-xs-12 required">
							<label for="iban"><s:message code="anagrafica.iban" /></label> <input
								type="text" class="form-control" name="iban" id="iban"
								value="${anagrafica.iban}" required>
						</div>
						<div class="form-group col-md-6 col-xs-12 required">
							<label class="d-block"><s:message
									code="anagrafica.incensurato" /></label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="incensurato_si"
									name="incensurato" value="true"
									${anagrafica.incensurato==true?'checked':''}> <label
									class="form-check-label" for="incensurato_si"><s:message
										code="button.si" /></label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="incensurato_no"
									value="false" name="incensurato"
									${anagrafica.incensurato==true?'checked':''}> <label
									class="form-check-label" for="incensurato_no"><s:message
										code="button.no" /></label>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12 pt-4 required">
							<label class="d-block"><s:message
									code="anagrafica.privacy" /></label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="privacy_si"
									name="privacy" value="true"
									${anagrafica.privacy==true?'checked':''}> <label
									class="form-check-label" for="privacy_si"><s:message
										code="button.si" /></label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" id="privacy_no"
									value="false" name="privacy"
									${anagrafica.privacy==false?'checked':''}> <label
									class="form-check-label" for="privacy_no"><s:message
										code="button.no" /></label>
							</div>

							<em class="text-muted d-block"><s:message
									code="anagrafica.title.privacy" /></em>
						</div>

					</div>
					<div class="form-row">
						<div class="form-group col-md-12 pt-4 pb-5">
							<button type="submit" class="btn btn-primary">
								<s:message code="button.salva" />
							</button>
						</div>
					</div>
					<input type="hidden" name="dataInsString"
						value="${anagrafica.dataInsString}"> <input type="hidden"
						name="dataUpdString" value="${anagrafica.dataUpdString}">
					<input type="hidden" name="userIns" value="${anagrafica.userIns}">
					<input type="hidden" name="userUpd" value="${anagrafica.userUpd}">
					<input type="hidden" name="idAnagrafica"
						value="${anagrafica.idAnagrafica}">
				</form>
			</div>
		</div>
	</div>
</body>

</html>