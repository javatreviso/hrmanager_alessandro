<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="h-100">
<head>
<meta charset="ISO-8859-1">
<title>HrManager</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<script type="text/javascript"
	src='<s:url value="/js/myPage.js"></s:url>'></script>

<style type="text/css">
	iframe #cshero-header{
		display:none;
	}
</style>

</head>

<s:url value="/anagrafica/insertForm" var="insert"></s:url>
<s:url value="/anagrafica/updateForm" var="update"></s:url>
<s:url value="/anagrafica/delete" var="delete"></s:url>



<body class="bg-white h-100">

	<nav
		class="navbar navbar-expand-lg navbar-light shadow-sm bg-white fixed-top bg-white">
		<a class="navbar-brand" href="#"> <img
			src="https://www.betacom.it/wp-content/uploads/2018/10/logo_betacom-2.png"
			height="30" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarText" aria-controls="navbarText"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="<s:url value=""/>"><s:message code="menu.home" /></a></li>
				<li class="nav-item"><a class="nav-link"
					href="<s:url value="/anagrafica"/>"><s:message
							code="menu.anagrafica" /></a></li>
				<li class="nav-item"><a class="nav-link"
					href="<s:url value="/utenze"/>"><s:message code="menu.utenze" /></a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <s:message code="menu.system" />
				</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<s:url value="/stati"/>"><s:message
								code="menu.system.stati" /></a> <a class="dropdown-item"
							href="<s:url value="/province"/>"><s:message
								code="menu.system.province" /></a> <a class="dropdown-item"
							href="<s:url value="/citta"/>"><s:message
								code="menu.system.citta" /></a>
					</div></li>
			</ul>
			<a class="navbar-text nav-link" href="/logout"> Logout </a>
		</div>
	</nav>

	<nav class="navbar">
		<a class="navbar-brand" href="#"> <img
			src="https://www.betacom.it/wp-content/uploads/2018/10/logo_betacom-2.png"
			height="30" alt="">
		</a>
	</nav>
	<div class="container-fluid bg-white p-0 h-100">
		<div class="row h-100">
			<div class="col-12 h-100">
				<iframe class="h-100 w-100" src="https://www.betacom.it"></iframe>
			</div>
		</div>
	</div>
</body>
</html>