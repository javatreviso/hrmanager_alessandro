<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HrManager</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<s:url value="/css/custom.css"></s:url>">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

</head>

<c:choose>
	<c:when test="${mode == 'insert'}">
		<s:url value="/stati/insert" var="action"></s:url>
	</c:when>
	<c:when test="${mode == 'update'}">
		<s:url value="/stati/update" var="action"></s:url>
	</c:when>
</c:choose>

<body class="bg-white">

	<nav
		class="navbar navbar-expand-lg navbar-light shadow-sm bg-white fixed-top"
		style="opacity: 0.8"
		>
		<a class="navbar-brand" href="#"> <img
			src="https://www.betacom.it/wp-content/uploads/2018/10/logo_betacom-2.png"
			height="30" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarText" aria-controls="navbarText"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link"	href="<s:url value="/homepage"/>">
					<s:message code="menu.home" /></a>
				</li>
				<li class="nav-item"><a class="nav-link"
					href="<s:url value="/anagrafica"/>"><s:message
							code="menu.anagrafica" /></a></li>
				<li class="nav-item active"><a class="nav-link"
					href="<s:url value="/utenze"/>"><s:message code="menu.utenze" /></a>
				</li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <s:message code="menu.system" />
				</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<s:url value="/stati"/>"><s:message
								code="menu.system.stati" /></a> <a class="dropdown-item"
							href="<s:url value="/province"/>"><s:message
								code="menu.system.province" /></a> <a class="dropdown-item"
							href="<s:url value="/citta"/>"><s:message
								code="menu.system.citta" /></a>
					</div></li>
			</ul>
			<a class="navbar-text nav-link" href="/logout"> Logout </a>
		</div>
	</nav>

	
	<div class="jumbotron jumbotron-fluid text-white"
		style="background-position: center center;background-size:cover;background-image: url('https://image.jimcdn.com/app/cms/image/transf/none/path/s95db0978189a0c1b/backgroundarea/ifb7ee04e0334596a/version/1535857777/image.jpg'); background-position: center center;">
		<div class="container mb-4 pt-5 text-center" style="text-shadow: 1px 1px 2px #000000;">

			<c:choose>
				<c:when test="${mode == 'insert'}">
					<h1 class="display-4 font-weight-bold">
						<s:message code="menu.system" />: <s:message code="menu.system.stati" /> - <s:message code="button.inserisci" />
					</h1>
				</c:when>
				<c:when test="${mode == 'update'}">
					<h1 class="display-4 font-weight-bold">
						<s:message code="menu.system" />: <s:message code="menu.system.stati" /> - <s:message code="button.modifica" />
					</h1>
				</c:when>
			</c:choose>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 bg-white mt-4 mb-4 pt-4 rounded border shadow">
				<form action="${action}" method="post">
					<div class="form-row">
						<div class="form-group col-md-12 col-xs-12 required">
							<label for="descrizione"><s:message code="stati.descrizione"/></label>
							<input type="text" class="form-control" name="descrizione"
								id="username" value="${stati.descrizione}"  required>
						</div>
					</div>
					
					
					<div class="form-row">
						<div class="form-group col-md-12 pt-4 pb-5">
							<button type="submit" class="btn btn-primary">
								<s:message code="button.salva" />
							</button>
						</div>
					</div>
					<input type="hidden" name="idStato" value="${stati.idStato}">
				</form>
			</div>
		</div>
	</div>
</body>

</html>