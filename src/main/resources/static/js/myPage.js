function verificaCampi() {
    var errors = false;

    var nome = document.getElementById('nome');
    if (nome.value == '') {
        errors = true;
        //nome.style.borderColor='red';
        nome.className = 'red';
        //alert("Errore, campo nome vuoto!");
    }

    var cognome = document.getElementById('cognome');
    if (cognome.value == '') {
        errors = true;
        //cognome.style.borderColor='red';
        cognome.className = 'red';
        //alert("Errore, campo cognome vuoto!");
    }

    var dataNascita = document.getElementById('dataNascitaString');
    if (dataNascita.value == '') {
        errors = true;
        dataNascita.className = 'red';
        //alert("Errore, campo data di nascita vuoto!");
    }

    var sesso = document.getElementById('sesso');
    if (sesso.value == '') {
        errors = true;
        sesso.className = 'red';
        //alert("Errore, campo sesso vuoto!");
    }

    var privacy = document.getElementById('privacy');
    if (privacy.checked == false) {
        errors = true;
        var labels = document.getElementsByTagName('label');
        for (var i = 0; i < labels.length; i++) {
            if (labels[i].htmlFor == 'privacy') {
                labels[i].style.color = 'red';
                break;
            }
        }

        //alert("Errore, per poter procedere è necessario accettare le condizioni sulla privacy");
    }

    if (errors == true) {
        //alert('Completare tutti i campi per procedere');
        $('#messaggio').text('Completare tutti i campi per procedere');
        $('#messaggio').parent('div').show();
    } else {

        return confirm('Sei sicuro di voler procedere?');
    }

    return !errors;
}


function provaAjax(){
    
   
    $.ajax({
        url:'http://localhost:8080/HRManager/rest/anagrafica',
        success:function(data){
            
            var select = $('#anagrafica');
            
            for(var i=0; i<data.length; i++){
                var option = $('<option/>');
                option.val(data[i].idAnagrafica);

                var cognomeNome = data[i].cognome + " " + data[i].nome;
                option.text(cognomeNome);

                select.append(option);
            }
        },
        error:function(){
            console.log('errore');
        }
    });

}

function getStati(){
    $('#province').empty();
    $('#citta').empty();
    $.ajax({
        url:'http://localhost:8080/HRManager/rest/stati',
        success:function(data){
            
            var select = $('#stati');
            select.empty();

            var optionVuota = $('<option/>');
            select.append(optionVuota);

            for(var i=0; i<data.length; i++){
                var option = $('<option/>');
                option.val(data[i].idStato);
                option.text(data[i].descrizione);
                select.append(option);
            }
        },
        error:function(){
            console.log('errore');
        }
    });
}

function getProvince(idStato){

    var url = 'http://localhost:8080/HRManager/rest/stati/'+idStato+'/province';

    $.ajax({
        url:url,
        success:function(data){
            
            var select = $('#province');
            select.empty();

            var optionVuota = $('<option/>');
            select.append(optionVuota);

            for(var i=0; i<data.length; i++){
                var option = $('<option/>');
                option.val(data[i].idProvincia);
                option.text(data[i].descrizione);
                select.append(option);
            }
        },
        error:function(){
            console.log('errore');
        }
    });
}

function getCitta(idProvincia){

    var url = 'http://localhost:8080/HRManager/rest/province/'+idProvincia+'/citta';

    $.ajax({
        url:url,
        success:function(data){
            
            var select = $('#citta');
            select.empty();

            var optionVuota = $('<option/>');
            select.append(optionVuota);

            for(var i=0; i<data.length; i++){
                var option = $('<option/>');
                option.val(data[i].idCitta);
                option.text(data[i].descrizione);
                select.append(option);
            }
        },
        error:function(){
            console.log('errore');
        }
    });
}