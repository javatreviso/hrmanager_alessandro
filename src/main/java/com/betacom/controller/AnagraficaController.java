package com.betacom.controller;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.betacom.beans.Anagrafica;
import com.betacom.services.AnagraficaService;

@Controller
@RequestMapping("/anagrafica")
public class AnagraficaController {	
	
	private final String _FORM = "anagrafica/anagrafica_form";
	private final String _LIST = "anagrafica/anagrafica_list";
	
	@Resource
	private AnagraficaService anagraficaService;//commento prova
	
	@Resource
	private MessageSource messageSource;
	
	
	@GetMapping("/insertForm")
	public String insertForm(Model model){
		model.addAttribute("mode", "insert");
		return _FORM;
	}
	
	@PostMapping("/insert")
	public String insert(Anagrafica anag,Model model, Locale locale){
		anagraficaService.insert(anag);
		
		String[] dati = new String[] {anag.getCognome(),anag.getNome()};
		String messaggio = messageSource.getMessage("anagrafica.message.insert.ok", dati, locale);
		model.addAttribute("messaggio",messaggio);
		return this.selectList(model);
	}
	
	@GetMapping("/updateForm/{idAnagrafica}")
	public String update(Model model,@PathVariable("idAnagrafica") Integer idAnagrafica) {
		Anagrafica anagrafica = anagraficaService.read(idAnagrafica);
		model.addAttribute("anagrafica", anagrafica);
		model.addAttribute("mode", "update");
		return _FORM;
	}
	
	@PostMapping("/update")
	public String update(Anagrafica anag,Model model){
		anagraficaService.update(anag);
		return "redirect:/anagrafica";
		//return this.selectList(model);
	}
	
	@GetMapping("/delete/{idAnagrafica}")
	public String delete(@PathVariable("idAnagrafica") Integer idAnagrafica,Model model) {
		anagraficaService.delete(idAnagrafica);
		return this.selectList(model);
	}
	
	@GetMapping("/select/{idAnagrafica}")
	public String selectSingle(	@PathVariable("idAnagrafica") Integer idAnagrafica,
								Model model) {		
		Anagrafica anagrafica = anagraficaService.read(idAnagrafica);		
		model.addAttribute("anag", anagrafica);		
		return _FORM;
	}
	
	@RequestMapping("/select/{cognome}/{nome}")
	public String selectSingle(	@PathVariable("cognome") String cognome,
								@PathVariable("nome") String nome,
								HttpServletRequest request,Model model) {
		
		return null;
	}
	
	@RequestMapping({"","/select"})
	public String selectList(Model model) {
		ArrayList<Anagrafica> listaAnagrafica = anagraficaService.read();
		model.addAttribute("listaAnagrafica",listaAnagrafica);
		return _LIST;
	}
	
	
}
