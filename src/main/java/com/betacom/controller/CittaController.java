package com.betacom.controller;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.betacom.beans.Citta;
import com.betacom.beans.Province;
import com.betacom.services.CittaService;
import com.betacom.services.ProvinceService;

@Controller
@RequestMapping("/citta")
public class CittaController {	
	
	private final String _FORM = "citta/citta_form";
	private final String _LIST = "citta/citta_list";
	
	@Resource
	private CittaService cittaService;
	
	@Resource
	private ProvinceService provinceService;
	
	@Resource
	private MessageSource messageSource;
	
	
	@GetMapping("/insertForm")
	public String insertForm(Model model){
		ArrayList<Province> listaProvince = provinceService.read();
		model.addAttribute("listaProvince",listaProvince);
		model.addAttribute("mode", "insert");
		return _FORM;
	}
	
	@PostMapping("/insert")
	public String insert(Citta citta,Model model, Locale locale){
		cittaService.insert(citta);
		
		String[] dati = new String[] {citta.getDescrizione()};
		String messaggio = messageSource.getMessage("citta.message.insert.ok", dati, locale);
		model.addAttribute("messaggio",messaggio);
		return this.selectList(model);
	}
	
	@GetMapping("/updateForm/{idCitta}")
	public String update(Model model,@PathVariable("idCitta") Integer idCitta) {
		Citta citta = cittaService.read(idCitta);
		ArrayList<Province> listaProvince = provinceService.read();
		model.addAttribute("listaProvince",listaProvince);
		model.addAttribute("citta", citta);
		model.addAttribute("mode", "update");
		return _FORM;
	}
	
	@PostMapping("/update")
	public String update(Citta citta,Model model){
		cittaService.update(citta);	
		return this.selectList(model);
	}
	
	@GetMapping("/delete/{idCitta}")
	public String delete(@PathVariable("idCitta") Integer idCitta,Model model) {
		cittaService.delete(idCitta);
		return this.selectList(model);
	}
	
	@GetMapping("/select/{idCitta}")
	public String selectSingle(	@PathVariable("idCitta") Integer idCitta,
								Model model) {		
		Citta citta = cittaService.read(idCitta);		
		model.addAttribute("citta", citta);		
		return _FORM;
	}
	
	
	@RequestMapping({"","/select"})
	public String selectList(Model model) {
		ArrayList<Citta> listaCitta = cittaService.read();
		model.addAttribute("listaCitta",listaCitta);
		return _LIST;
	}
	
	
}
