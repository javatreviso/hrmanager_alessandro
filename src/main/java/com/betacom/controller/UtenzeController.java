package com.betacom.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.betacom.beans.Anagrafica;
import com.betacom.beans.Utenze;
import com.betacom.services.AnagraficaService;
import com.betacom.services.UtenzeService;

@Controller
@RequestMapping("/utenze")
public class UtenzeController {	
	
	private final String _FORM = "utenze/utenze_form";
	private final String _LIST = "utenze/utenze_list";
	
	@Resource
	private UtenzeService utenzeService;
	@Resource
	private AnagraficaService anagraficaService;
	
	@Resource
	private MessageSource messageSource;
	
	
	@GetMapping("/insertForm")
	public String insertForm(Model model){
		
		Utenze utenze = new Utenze();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 1);
		utenze.setDataScadenza(c.getTime());
		model.addAttribute("utenze", utenze);
		model.addAttribute("mode", "insert");
		ArrayList<Anagrafica> anagraficaList = anagraficaService.read();
		model.addAttribute("anagraficaList",anagraficaList);
		return _FORM;
	}
	
	@PostMapping("/insert")
	public String insert(Utenze utenze,Model model, Locale locale){
		utenzeService.insert(utenze);
		String[] dati = new String[] {utenze.getUsername()};
		String messaggio = messageSource.getMessage("utenze.message.insert.ok", dati, locale);
		model.addAttribute("messaggio",messaggio);
		return this.selectList(model);
	}
	
	@GetMapping("/updateForm/{username}")
	public String update(Model model,@PathVariable("username") String username) {
		Utenze utenze = utenzeService.read(username);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 1);
		utenze.setDataScadenza(c.getTime());
		model.addAttribute("utenze", utenze);
		model.addAttribute("mode", "update");
		ArrayList<Anagrafica> anagraficaList = anagraficaService.read();
		model.addAttribute("anagraficaList",anagraficaList);
		return _FORM;
	}
	
	@PostMapping("/update")
	public String update(Utenze utenze,Model model){
		utenzeService.update(utenze);	
		return this.selectList(model);
	}
	
	@GetMapping("/delete/{username}")
	public String delete(@PathVariable("username") String username,Model model) {
		utenzeService.delete(username);
		return this.selectList(model);
	}
	
	@GetMapping("/select/{username}")
	public String selectSingle(	@PathVariable("username") String username,
								Model model) {		
		Utenze utenze = utenzeService.read(username);		
		model.addAttribute("utenze", utenze);		
		return _FORM;
	}
	

	@RequestMapping({"","/select"})
	public String selectList(Model model) {
		ArrayList<Utenze> listaUtenze = utenzeService.read();
		model.addAttribute("listaUtenze",listaUtenze);
		return _LIST;
	}	
	
}
