package com.betacom.controller;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.betacom.beans.Stati;
import com.betacom.services.StatiService;

@Controller
@RequestMapping("/stati")
public class StatiController {	
	
	private final String _FORM = "stati/stati_form";
	private final String _LIST = "stati/stati_list";
	
	@Resource
	private StatiService statiService;
	
	@Resource
	private MessageSource messageSource;
	
	
	@GetMapping("/insertForm")
	public String insertForm(Model model){
		model.addAttribute("mode", "insert");
		return _FORM;
	}
	
	@PostMapping("/insert")
	public String insert(Stati stati,Model model, Locale locale){
		statiService.insert(stati);
		
		String[] dati = new String[] {stati.getDescrizione()};
		String messaggio = messageSource.getMessage("stati.message.insert.ok", dati, locale);
		model.addAttribute("messaggio",messaggio);
		return this.selectList(model);
	}
	
	@GetMapping("/updateForm/{idStati}")
	public String update(Model model,@PathVariable("idStati") Integer idStati) {
		Stati stati = statiService.read(idStati);
		model.addAttribute("stati", stati);
		model.addAttribute("mode", "update");
		return _FORM;
	}
	
	@PostMapping("/update")
	public String update(Stati stati,Model model){
		statiService.update(stati);	
		return this.selectList(model);
	}
	
	@GetMapping("/delete/{idStati}")
	public String delete(@PathVariable("idStati") Integer idStati,Model model) {
		statiService.delete(idStati);
		return this.selectList(model);
	}
	
	@GetMapping("/select/{idStati}")
	public String selectSingle(	@PathVariable("idStati") Integer idStati,
								Model model) {		
		Stati stati = statiService.read(idStati);		
		model.addAttribute("stati", stati);		
		return _FORM;
	}
	
	
	@RequestMapping({"","/select"})
	public String selectList(Model model) {
		ArrayList<Stati> listaStati = statiService.read();
		model.addAttribute("listaStati",listaStati);
		return _LIST;
	}
	
	
}
