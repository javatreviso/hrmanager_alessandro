package com.betacom.controller;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.betacom.beans.Province;
import com.betacom.beans.Stati;
import com.betacom.services.ProvinceService;
import com.betacom.services.StatiService;

@Controller
@RequestMapping("/province")
public class ProvinceController {	
	
	private final String _FORM = "province/province_form";
	private final String _LIST = "province/province_list";
	
	@Resource
	private ProvinceService provinceService;
	
	@Resource
	private StatiService statiService;
	
	@Resource
	private MessageSource messageSource;
	
	
	@GetMapping("/insertForm")
	public String insertForm(Model model){
		ArrayList<Stati> listaStati = statiService.read();
		model.addAttribute("listaStati",listaStati);
		model.addAttribute("mode", "insert");
		return _FORM;
	}
	
	@PostMapping("/insert")
	public String insert(Province province,Model model, Locale locale){
		provinceService.insert(province);
		
		String[] dati = new String[] {province.getDescrizione()};
		String messaggio = messageSource.getMessage("province.message.insert.ok", dati, locale);
		model.addAttribute("messaggio",messaggio);
		return this.selectList(model);
	}
	
	@GetMapping("/updateForm/{idProvince}")
	public String update(Model model,@PathVariable("idProvince") Integer idProvince) {
		Province province = provinceService.read(idProvince);
		ArrayList<Stati> listaStati = statiService.read();
		model.addAttribute("listaStati",listaStati);
		model.addAttribute("province", province);
		model.addAttribute("mode", "update");
		return _FORM;
	}
	
	@PostMapping("/update")
	public String update(Province province,Model model){
		provinceService.update(province);	
		return this.selectList(model);
	}
	
	@GetMapping("/delete/{idProvince}")
	public String delete(@PathVariable("idProvince") Integer idProvince,Model model) {
		provinceService.delete(idProvince);
		return this.selectList(model);
	}
	
	@GetMapping("/select/{idProvince}")
	public String selectSingle(	@PathVariable("idProvince") Integer idProvince,
								Model model) {		
		Province province = provinceService.read(idProvince);		
		model.addAttribute("province", province);		
		return _FORM;
	}
	
	
	@RequestMapping({"","/select"})
	public String selectList(Model model) {
		ArrayList<Province> listaProvince = provinceService.read();
		model.addAttribute("listaProvince",listaProvince);
		return _LIST;
	}
	
	
}
