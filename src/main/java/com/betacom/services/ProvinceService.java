package com.betacom.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.betacom.beans.Province;
import com.betacom.jpa.entities.ProvinceEntity;
import com.betacom.jpa.repostitories.ProvinceJpaRepository;
import com.betacom.utils.HRManagerUtils;

@Component
@Transactional
public class ProvinceService {

	@Resource
	private ProvinceJpaRepository provinceJpaRepository;

	public ArrayList<Province> read() {
		List<ProvinceEntity> list = provinceJpaRepository.findAll();
		ArrayList<Province> beanList = new ArrayList<Province>();

		for (ProvinceEntity provinceEntity : list) {
			Province province = HRManagerUtils.convertToBean(provinceEntity);
			beanList.add(province);
		}

		return beanList;
	}

	public Province read(Integer idProvince) {
		ProvinceEntity provinceEntity = provinceJpaRepository.getOne(idProvince);
		Province province = HRManagerUtils.convertToBean(provinceEntity);
		return province;
	}

	public Province insert(Province province) {
		ProvinceEntity provinceEntity = HRManagerUtils.convertToEntity(province);
		ProvinceEntity provinceEntitySaved = provinceJpaRepository.save(provinceEntity);
		return HRManagerUtils.convertToBean(provinceEntitySaved);
	}

	public Province update(Province province) {
		ProvinceEntity provinceEntity = HRManagerUtils.convertToEntity(province);
		ProvinceEntity provinceEntitySaved = provinceJpaRepository.save(provinceEntity);
		return HRManagerUtils.convertToBean(provinceEntitySaved);
	}

	public void delete(Integer idProvincia) {
		provinceJpaRepository.deleteById(idProvincia);
	}
}
