package com.betacom.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.betacom.beans.Citta;
import com.betacom.jpa.entities.CittaEntity;
import com.betacom.jpa.repostitories.CittaJpaRepository;
import com.betacom.utils.HRManagerUtils;

@Component
@Transactional
public class CittaService {

	@Resource
	private CittaJpaRepository cittaJpaRepository;

	public ArrayList<Citta> read() {
		List<CittaEntity> list = cittaJpaRepository.findAll();
		ArrayList<Citta> beanList = new ArrayList<Citta>();

		for (CittaEntity cittaEntity : list) {
			Citta citta = HRManagerUtils.convertToBean(cittaEntity);
			beanList.add(citta);
		}

		return beanList;
	}

	public Citta read(Integer idCitta) {
		CittaEntity cittaEntity = cittaJpaRepository.getOne(idCitta);
		Citta citta = HRManagerUtils.convertToBean(cittaEntity);
		return citta;
	}

	public Citta insert(Citta citta) {
		CittaEntity cittaEntity = HRManagerUtils.convertToEntity(citta);
		CittaEntity cittaEntitySaved = cittaJpaRepository.save(cittaEntity);
		return HRManagerUtils.convertToBean(cittaEntitySaved);
	}

	public Citta update(Citta citta) {
		CittaEntity cittaEntity = HRManagerUtils.convertToEntity(citta);
		CittaEntity cittaEntitySaved = cittaJpaRepository.save(cittaEntity);
		return HRManagerUtils.convertToBean(cittaEntitySaved);
	}

	public void delete(Integer idCitta) {
		cittaJpaRepository.deleteById(idCitta);
	}
}
