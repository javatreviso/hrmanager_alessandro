package com.betacom.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.betacom.beans.Stati;
import com.betacom.jpa.entities.StatiEntity;
import com.betacom.jpa.repostitories.StatiJpaRepository;
import com.betacom.utils.HRManagerUtils;

@Component
@Transactional
public class StatiService {

	@Resource
	private StatiJpaRepository statiJpaRepository;

	public ArrayList<Stati> read() {
		List<StatiEntity> list = statiJpaRepository.findAll();
		ArrayList<Stati> beanList = new ArrayList<Stati>();

		for (StatiEntity statiEntity : list) {
			Stati stati = HRManagerUtils.convertToBean(statiEntity);
			beanList.add(stati);
		}

		return beanList;
	}

	public Stati read(Integer idStati) {
		StatiEntity statiEntity = statiJpaRepository.getOne(idStati);
		Stati stati = HRManagerUtils.convertToBean(statiEntity);
		return stati;
	}

	public Stati insert(Stati stati) {
		StatiEntity statiEntity = HRManagerUtils.convertToEntity(stati);
		StatiEntity statiEntitySaved = statiJpaRepository.save(statiEntity);
		return HRManagerUtils.convertToBean(statiEntitySaved);
	}

	public Stati update(Stati stati) {
		StatiEntity statiEntity = HRManagerUtils.convertToEntity(stati);
		StatiEntity statiEntitySaved = statiJpaRepository.save(statiEntity);
		return HRManagerUtils.convertToBean(statiEntitySaved);
	}

	public void delete(Integer idStati) {
		statiJpaRepository.deleteById(idStati);
	}
}
