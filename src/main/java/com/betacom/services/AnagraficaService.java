package com.betacom.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.betacom.beans.Anagrafica;
import com.betacom.jpa.entities.AnagraficaEntity;
import com.betacom.jpa.repostitories.AnagraficaJpaRepository;
import com.betacom.utils.HRManagerUtils;

@Component
@Transactional
public class AnagraficaService {

	@Resource
	private AnagraficaJpaRepository anagraficaJpaRepository;

	public ArrayList<Anagrafica> read() {
		List<AnagraficaEntity> list = anagraficaJpaRepository.findAll();
		ArrayList<Anagrafica> beanList = new ArrayList<Anagrafica>();

		for (AnagraficaEntity anagraficaEntity : list) {
			Anagrafica anagrafica = HRManagerUtils.convertToBean(anagraficaEntity);
			beanList.add(anagrafica);
		}

		return beanList;
	}

	public ArrayList<Anagrafica> read(String cognome) {
		List<AnagraficaEntity> list = anagraficaJpaRepository.findByCognome(cognome);
		ArrayList<Anagrafica> beanList = new ArrayList<Anagrafica>();

		for (AnagraficaEntity anagraficaEntity : list) {
			Anagrafica anagrafica = HRManagerUtils.convertToBean(anagraficaEntity);
			beanList.add(anagrafica);
		}

		return beanList;
	}

	public Anagrafica read(Integer idAnagrafica) {
		AnagraficaEntity anagraficaEntity = anagraficaJpaRepository.getOne(idAnagrafica);
		Anagrafica anagrafica = HRManagerUtils.convertToBean(anagraficaEntity);
		return anagrafica;
	}

	public Anagrafica insert(Anagrafica anagrafica) {
		AnagraficaEntity anagraficaEntity = HRManagerUtils.convertToEntity(anagrafica);
		anagraficaEntity.setUserIns("system");
		anagraficaEntity.setDataIns(new Date());
		AnagraficaEntity anagraficaEntitySaved = anagraficaJpaRepository.save(anagraficaEntity);
		return HRManagerUtils.convertToBean(anagraficaEntitySaved);
	}

	public Anagrafica update(Anagrafica anagrafica) {
		AnagraficaEntity anagraficaEntity = HRManagerUtils.convertToEntity(anagrafica);
		anagraficaEntity.setUserUpd("system");
		anagraficaEntity.setDataUpd(new Date());
		AnagraficaEntity anagraficaEntitySaved = anagraficaJpaRepository.save(anagraficaEntity);
		return HRManagerUtils.convertToBean(anagraficaEntitySaved);
	}

	public void delete(Integer idAnagrafica) {
		anagraficaJpaRepository.deleteById(idAnagrafica);
	}
}
