package com.betacom.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.betacom.beans.Utenze;
import com.betacom.jpa.entities.UtenzeEntity;
import com.betacom.jpa.repostitories.UtenzeJpaRepository;
import com.betacom.utils.HRManagerUtils;

@Component
@Transactional
public class UtenzeService {

	@Resource
	private UtenzeJpaRepository utenzeJpaRepository;
	
	@Resource
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public ArrayList<Utenze> read() {
		List<UtenzeEntity> list = utenzeJpaRepository.findAll();
		ArrayList<Utenze> beanList = new ArrayList<Utenze>();

		for (UtenzeEntity utenzeEntity : list) {
			Utenze utenze = HRManagerUtils.convertToBean(utenzeEntity);
			beanList.add(utenze);
		}

		return beanList;
	}


	public Utenze read(String username) {
		UtenzeEntity utenzeEntity = utenzeJpaRepository.getOne(username);
		Utenze utenze = HRManagerUtils.convertToBean(utenzeEntity);
		return utenze;
	}

	public Utenze insert(Utenze utenze) {
		UtenzeEntity utenzeEntity = HRManagerUtils.convertToEntity(utenze);
		String passwordCodificata = bCryptPasswordEncoder.encode(utenze.getPassword());
			
		utenzeEntity.setPassword(passwordCodificata);
		utenzeEntity.setUserIns("system");
		utenzeEntity.setDataIns(new Date());
		UtenzeEntity utenzeEntitySaved = utenzeJpaRepository.save(utenzeEntity);
		return HRManagerUtils.convertToBean(utenzeEntitySaved);
	}

	public Utenze update(Utenze utenze) {
		UtenzeEntity utenzeEntity = HRManagerUtils.convertToEntity(utenze);
		utenzeEntity.setUserUpd("system");
		utenzeEntity.setDataUpd(new Date());
		UtenzeEntity utenzeEntitySaved = utenzeJpaRepository.save(utenzeEntity);
		return HRManagerUtils.convertToBean(utenzeEntitySaved);
	}

	public void delete(String username) {
		utenzeJpaRepository.deleteById(username);
	}

	
}
