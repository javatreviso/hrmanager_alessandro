package com.betacom.jpa.repostitories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.betacom.jpa.entities.AnagraficaEntity;

public interface AnagraficaJpaRepository extends JpaRepository<AnagraficaEntity, Integer>{
	List<AnagraficaEntity> findByCognome(String cognome);
	AnagraficaEntity findByCognomeAndNome (String cognome, String nome);
	AnagraficaEntity findByCognomeOrNome (String cognome, String nome);
	AnagraficaEntity findByCognomeIsNull ();
	AnagraficaEntity findByCognomeIsNotNull ();
	AnagraficaEntity findByCognomeIsNotNullAndNome (String nome);
	List<AnagraficaEntity> findByCognomeIn (List<String> cognome);
	List<AnagraficaEntity> findByCognomeNotIn (List<String> cognome);
	
	
	List<AnagraficaEntity> findByCognomeOrderByNomeDescCognomeAsc(String cognome);
}
