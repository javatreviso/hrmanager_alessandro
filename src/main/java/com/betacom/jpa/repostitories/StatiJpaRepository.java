package com.betacom.jpa.repostitories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.betacom.jpa.entities.StatiEntity;

public interface StatiJpaRepository extends JpaRepository<StatiEntity, Integer>{
}
