package com.betacom.jpa.repostitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.betacom.jpa.entities.CittaEntity;

public interface CittaJpaRepository extends JpaRepository<CittaEntity, Integer>{
}
