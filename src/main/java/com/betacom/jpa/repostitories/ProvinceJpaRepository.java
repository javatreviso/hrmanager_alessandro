package com.betacom.jpa.repostitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.betacom.jpa.entities.ProvinceEntity;

public interface ProvinceJpaRepository extends JpaRepository<ProvinceEntity, Integer>{
}
