package com.betacom.jpa.repostitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.betacom.jpa.entities.UtenzeEntity;

public interface UtenzeJpaRepository extends JpaRepository<UtenzeEntity, String>{	
}
