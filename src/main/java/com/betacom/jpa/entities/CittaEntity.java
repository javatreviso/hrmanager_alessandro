package com.betacom.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="citta", catalog="hr_manager")
public class CittaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_citta")
	private Integer idCitta;
	
	@Column(name="descrizione")
	private String 	descrizione;
	

	@ManyToOne(optional=false)
	@JoinColumn(name = "id_provincia")
	private ProvinceEntity provincia;
	
	public ProvinceEntity getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinceEntity provincia) {
		this.provincia = provincia;
	}

	public CittaEntity() {
		super();
	}

	public CittaEntity(Integer idCitta, String descrizione) {
		super();
		this.idCitta = idCitta;
		this.descrizione = descrizione;
	}
	
	public Integer getIdCitta() {
		return idCitta;
	}
	public void setIdCitta(Integer idCitta) {
		this.idCitta = idCitta;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
}
