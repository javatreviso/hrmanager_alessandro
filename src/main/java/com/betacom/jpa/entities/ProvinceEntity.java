package com.betacom.jpa.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="province", catalog="hr_manager")
public class ProvinceEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_provincia")
	private Integer idProvincia;
	
	@Column(name="descrizione")
	private String 	descrizione;
	

	@ManyToOne(optional=false)
	@JoinColumn(name = "id_stato")
	private StatiEntity stato;
	
	@OneToMany(mappedBy = "provincia")
	private List<CittaEntity> listaCitta;
	
	public StatiEntity getStato() {
		return stato;
	}

	public void setStato(StatiEntity stato) {
		this.stato = stato;
	}

	public ProvinceEntity() {
		super();
	}

	public ProvinceEntity(Integer idProvincia, String descrizione) {
		super();
		this.idProvincia = idProvincia;
		this.descrizione = descrizione;
	}
	
	public Integer getIdProvincia() {
		return idProvincia;
	}
	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<CittaEntity> getListaCitta() {
		return listaCitta;
	}

	public void setListaCitta(List<CittaEntity> listaCitta) {
		this.listaCitta = listaCitta;
	}
}
