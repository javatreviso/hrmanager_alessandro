package com.betacom.jpa.entities;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "anagrafica", catalog = "hr_manager")
public class AnagraficaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_anagrafica")
	private Integer idAnagrafica;
	
	@NotNull
	@Column(name="cognome",length=50)
	private String 	cognome;
	
	@NotNull
	@Column(name="nome",length=50)
	private String	nome;
		
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name="data_nascita")
	private Date dataNascita;
	
	@NotNull
	@Column(name="sesso",length=1)
	private String sesso;
	
	@Column(name="telefono",length=15)
	private String telefono;
	
	@Column(name="mail",length=100)
	private String mail;
	
	@Column(name="stato_civile",length=15)
	private String statoCivile;
	
	@Column(name="cod_fiscale",length=20)
	private String codFiscale;
	
	@Column(name="iban",length=100)
	private String iban;
	
	@NotNull
	@Column(name="privacy", columnDefinition = "TINYINT")
	private Boolean privacy; 
	
	@NotNull
	@Column(name="incensurato", columnDefinition = "TINYINT")
	private Boolean incensurato;
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@Column(name="data_ins")
	private Date dataIns;
	
	@NotNull
	@Column(name="user_ins",length=8)
	private String userIns;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_upd")
	private Date dataUpd;
	
	@Column(name="user_upd",length=8)
	private String userUpd;
	
	@OneToOne(fetch = FetchType.LAZY,
            mappedBy = "anagrafica")
	private UtenzeEntity utenze;
	
	

	public AnagraficaEntity() {
		super();
	}	

	public AnagraficaEntity(Integer idAnagrafica, String cognome, String nome,
			Date dataNascita, String sesso, String telefono, String mail, String statoCivile,
			String codFiscale, String iban, Boolean privacy, Boolean incensurato,
			Date dataIns, String userIns, Date dataUpd, String userUpd) {
		super();
		this.idAnagrafica = idAnagrafica;
		this.cognome = cognome;
		this.nome = nome;
		this.dataNascita = dataNascita;
		this.sesso = sesso;
		this.telefono = telefono;
		this.mail = mail;
		this.statoCivile = statoCivile;
		this.codFiscale = codFiscale;
		this.iban = iban;
		this.privacy = privacy;
		this.incensurato = incensurato;
		this.dataIns = dataIns;
		this.userIns = userIns;
		this.dataUpd = dataUpd;
		this.userUpd = userUpd;
	}


	public Integer getIdAnagrafica() {
		return idAnagrafica;
	}

	public void setIdAnagrafica(Integer idAnagrafica) {
		this.idAnagrafica = idAnagrafica;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getStatoCivile() {
		return statoCivile;
	}

	public void setStatoCivile(String statoCivile) {
		this.statoCivile = statoCivile;
	}

	public String getCodFiscale() {
		return codFiscale;
	}

	public void setCodFiscale(String codFiscale) {
		this.codFiscale = codFiscale;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Boolean getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Boolean privacy) {
		this.privacy = privacy;
	}

	public Boolean getIncensurato() {
		return incensurato;
	}

	public void setIncensurato(Boolean incensurato) {
		this.incensurato = incensurato;
	}

	public Date getDataIns() {
		return dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public String getUserIns() {
		return userIns;
	}

	public void setUserIns(String userIns) {
		this.userIns = userIns;
	}

	public Date getDataUpd() {
		return dataUpd;
	}

	public void setDataUpd(Date dataUpd) {
		this.dataUpd = dataUpd;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}

	public UtenzeEntity getUtenze() {
		return utenze;
	}

	public void setUtenze(UtenzeEntity utenze) {
		this.utenze = utenze;
	}
	
	
	
}
