package com.betacom.jpa.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="stati", catalog="hr_manager")
public class StatiEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_stato")
	private Integer idStato;
	
	@Column(name="descrizione")
	private String 	descrizione;
	
	@OneToMany(mappedBy = "stato")
	private List<ProvinceEntity> listaProvince;

	
	
	public List<ProvinceEntity> getListaProvince() {
		return listaProvince;
	}

	public void setListaProvince(List<ProvinceEntity> listaProvince) {
		this.listaProvince = listaProvince;
	}

	public StatiEntity() {
		super();
	}

	public StatiEntity(Integer idStato, String descrizione) {
		super();
		this.idStato = idStato;
		this.descrizione = descrizione;
	}
	
	public Integer getIdStato() {
		return idStato;
	}
	public void setIdStato(Integer idStato) {
		this.idStato = idStato;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
