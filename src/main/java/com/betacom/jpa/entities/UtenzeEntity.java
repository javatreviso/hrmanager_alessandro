package com.betacom.jpa.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "utenze", catalog = "hr_manager")
public class UtenzeEntity {
	
	@Id
	@NotNull
	@Column(name="username", length=8)
	private String 	username;
	
	@NotNull
	@Column(name="password", length=255)
	private String 	password;
	
	@NotNull
	@Column(name="ruolo")
	private String ruolo;
	
	@NotNull
	@Column(name="attivo", columnDefinition="TINYINT")
	private Boolean  attivo;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name="dataScadenza")
	private Date		dataScadenza;
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@Column(name="data_ins")
	private Date dataIns;
	
	@NotNull
	@Column(name="user_ins",length=8)
	private String userIns;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_upd")
	private Date dataUpd;
	
	@Column(name="user_upd",length=8)
	private String userUpd;
	
	
	@OneToOne(optional = false)
    @JoinColumn(name = "id_anagrafica", nullable = false)
	AnagraficaEntity anagrafica;
	
	
	
	
	public UtenzeEntity() {
		super();
	}
	
	

	public UtenzeEntity(String username, String password, Integer idAnagrafica,
			Boolean attivo, Date dataScadenza, Date dataIns, String userIns,
			Date dataUpd, String userUpd, String ruolo) {
		super();
		this.username = username;
		this.password = password;
		AnagraficaEntity anagrafica = new AnagraficaEntity();
		anagrafica.setIdAnagrafica(idAnagrafica);
		this.anagrafica = anagrafica;
		this.attivo = attivo;
		this.dataScadenza = dataScadenza;
		this.dataIns = dataIns;
		this.userIns = userIns;
		this.dataUpd = dataUpd;
		this.userUpd = userUpd;
		this.ruolo = ruolo;
	}



	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getAttivo() {
		return attivo;
	}
	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}
	public Date getDataScadenza() {
		return dataScadenza;
	}
	public void setDataScadenza(Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public Date getDataIns() {
		return dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public String getUserIns() {
		return userIns;
	}

	public void setUserIns(String userIns) {
		this.userIns = userIns;
	}

	public Date getDataUpd() {
		return dataUpd;
	}

	public void setDataUpd(Date dataUpd) {
		this.dataUpd = dataUpd;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}

	public AnagraficaEntity getAnagrafica() {
		return anagrafica;
	}
	
	public void setAnagrafica(AnagraficaEntity anagrafica) {
		this.anagrafica = anagrafica;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}	
	
}
