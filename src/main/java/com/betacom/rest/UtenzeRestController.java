package com.betacom.rest;

import java.util.ArrayList;
import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.betacom.beans.Utenze;
import com.betacom.services.UtenzeService;

@RestController
public class UtenzeRestController {	
	
	private final String _URL =  "/rest/utenze";
	private final String _URL_PARAM =  _URL+"/{username}";
	
	@Resource
	private UtenzeService utenzeService;
	
	@Resource
	private MessageSource messageSource;
	
	@GetMapping(_URL) //       "/rest/utenze"
	@ResponseBody
	public ArrayList<Utenze> read() {
		ArrayList<Utenze> listaUtenze = utenzeService.read();
		return listaUtenze;
	}
	
	@GetMapping(_URL_PARAM)//       "/rest/utenze/{username}"
	@ResponseBody
	public Utenze read(	@PathVariable("username") String username) {		
		Utenze utenze = utenzeService.read(username);				
		return utenze;
	}
	
	@PostMapping(_URL)//			"/rest/utenze"
	@ResponseBody
	public Utenze create(Utenze utenze){
		Utenze utenzeSaved = utenzeService.insert(utenze);		
		return utenzeSaved;
	}
		
	@PutMapping(_URL)//       "/rest/utenze"
	@ResponseBody
	public Utenze update(Utenze utenze){
		Utenze utenzeSaved = utenzeService.update(utenze);	
		return utenzeSaved;
	}
	
	@DeleteMapping(_URL_PARAM)//       "/rest/utenze/{username}"
	@ResponseBody
	public void delete(@PathVariable("username") String username) {
		utenzeService.delete(username);
	}	
}
