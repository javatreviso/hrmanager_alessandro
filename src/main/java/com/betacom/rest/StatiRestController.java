package com.betacom.rest;

import java.util.ArrayList;
import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.betacom.beans.Province;
import com.betacom.beans.Stati;
import com.betacom.services.StatiService;

@RestController
public class StatiRestController {	
	
	private final String _URL =  "/rest/stati";
	private final String _URL_PARAM =  _URL+"/{idStati}";
	
	@Resource
	private StatiService statiService;
	
	@Resource
	private MessageSource messageSource;
	
	@CrossOrigin(origins="*")
	@GetMapping(_URL) //       "/rest/stati"
	@ResponseBody
	public ArrayList<Stati> read() {
		ArrayList<Stati> listaStati = statiService.read();
		return listaStati;
	}
	
	@GetMapping(_URL_PARAM)//       "/rest/stati/{idStati}"
	@ResponseBody
	public Stati read(	@PathVariable("idStati") Integer idStati) {		
		Stati stati = statiService.read(idStati);				
		return stati;
	}
	
	@CrossOrigin(origins="*")
	@GetMapping("/rest/stati/{idStati}/province")
	@ResponseBody
	public ArrayList<Province> readProvince(@PathVariable("idStati") Integer idStati) throws Exception{
		try {		
			Stati stati = statiService.read(idStati);
			return stati.getListaProvince();		
		}catch(Exception e) {
			throw new Exception("Stato con id "+idStati+" non trovato");
		}
	}
	
	
	@PostMapping(_URL)//			"/rest/stati"
	@ResponseBody
	public Stati create(Stati stati){
		Stati statiSaved = statiService.insert(stati);		
		return statiSaved;
	}
		
	@PutMapping(_URL)//       "/rest/stati"
	@ResponseBody
	public Stati update(Stati stati){
		Stati statiSaved = statiService.update(stati);	
		return statiSaved;
	}
	
	@DeleteMapping(_URL_PARAM)//       "/rest/stati/{idStati}"
	@ResponseBody
	public void delete(@PathVariable("idStati") Integer idStati) {
		statiService.delete(idStati);
	}	
}
