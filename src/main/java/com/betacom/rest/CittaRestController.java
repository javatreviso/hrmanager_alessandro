package com.betacom.rest;

import java.util.ArrayList;
import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.betacom.beans.Citta;
import com.betacom.services.CittaService;

@RestController
public class CittaRestController {	
	
	private final String _URL =  "/rest/citta";
	private final String _URL_PARAM =  _URL+"/{idCitta}";
	
	@Resource
	private CittaService cittaService;
	
	@Resource
	private MessageSource messageSource;
	
	@GetMapping(_URL) //       "/rest/citta"
	@ResponseBody
	public ArrayList<Citta> read() {
		ArrayList<Citta> listaCitta = cittaService.read();
		return listaCitta;
	}
	
	@GetMapping(_URL_PARAM)//       "/rest/citta/{idCitta}"
	@ResponseBody
	public Citta read(	@PathVariable("idCitta") Integer idCitta) {		
		Citta citta = cittaService.read(idCitta);				
		return citta;
	}
	
	@PostMapping(_URL)//			"/rest/citta"
	@ResponseBody
	public Citta create(Citta citta){
		Citta cittaSaved = cittaService.insert(citta);		
		return cittaSaved;
	}
		
	@PutMapping(_URL)//       "/rest/citta"
	@ResponseBody
	public Citta update(Citta citta){
		Citta cittaSaved = cittaService.update(citta);	
		return cittaSaved;
	}
	
	@DeleteMapping(_URL_PARAM)//       "/rest/citta/{idCitta}"
	@ResponseBody
	public void delete(@PathVariable("idCitta") Integer idCitta) {
		cittaService.delete(idCitta);
	}	
}
