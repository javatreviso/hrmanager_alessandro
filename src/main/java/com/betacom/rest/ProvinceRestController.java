package com.betacom.rest;

import java.util.ArrayList;
import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.betacom.beans.Citta;
import com.betacom.beans.Province;
import com.betacom.beans.Stati;
import com.betacom.services.ProvinceService;

@RestController
public class ProvinceRestController {	
	
	private final String _URL =  "/rest/province";
	private final String _URL_PARAM =  _URL+"/{idProvince}";
	
	@Resource
	private ProvinceService provinceService;
	
	@Resource
	private MessageSource messageSource;
	
	@GetMapping(_URL) //       "/rest/province"
	@ResponseBody
	public ArrayList<Province> read() {
		ArrayList<Province> listaProvince = provinceService.read();
		return listaProvince;
	}
	
	@GetMapping(_URL_PARAM)//       "/rest/province/{idProvince}"
	@ResponseBody
	public Province read(	@PathVariable("idProvince") Integer idProvince) {		
		Province province = provinceService.read(idProvince);				
		return province;
	}
	
	@CrossOrigin(origins="*")
	@GetMapping("/rest/province/{idProvincia}/citta")
	@ResponseBody
	public ArrayList<Citta> readCitta(@PathVariable("idProvincia") Integer idProvincia) throws Exception{
		try {		
			Province provincia = provinceService.read(idProvincia);
			return provincia.getListaCitta();		
		}catch(Exception e) {
			throw new Exception("Provincia con id "+idProvincia+" non trovata");
		}
	}
	
	@PostMapping(_URL)//			"/rest/province"
	@ResponseBody
	public Province create(Province province){
		Province provinceSaved = provinceService.insert(province);		
		return provinceSaved;
	}
		
	@PutMapping(_URL)//       "/rest/province"
	@ResponseBody
	public Province update(Province province){
		Province provinceSaved = provinceService.update(province);	
		return provinceSaved;
	}
	
	@DeleteMapping(_URL_PARAM)//       "/rest/province/{idProvince}"
	@ResponseBody
	public void delete(@PathVariable("idProvince") Integer idProvince) {
		provinceService.delete(idProvince);
	}	
}
