package com.betacom.rest;

import java.util.ArrayList;
import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.betacom.beans.Anagrafica;
import com.betacom.services.AnagraficaService;

@RestController
public class AnagraficaRestController {	
	
	private final String _URL =  "/rest/anagrafica";
	private final String _URL_PARAM =  _URL+"/{idAnagrafica}";
	
	@Resource
	private AnagraficaService anagraficaService;
	
	@Resource
	private MessageSource messageSource;
	
	@GetMapping(_URL)
	@CrossOrigin(origins="*")
	@ResponseBody
	public ArrayList<Anagrafica> read() {
		ArrayList<Anagrafica> listaAnagrafica = anagraficaService.read();
		return listaAnagrafica;
	}
	
	@GetMapping(_URL_PARAM)//       "/rest/anagrafica/{idAnagrafica}"
	@ResponseBody
	public Anagrafica read(	@PathVariable("idAnagrafica") Integer idAnagrafica) {		
		Anagrafica anagrafica = anagraficaService.read(idAnagrafica);				
		return anagrafica;
	}
	
	@PostMapping(_URL)//			"/rest/anagrafica"
	@ResponseBody
	public Anagrafica create(Anagrafica anagrafica){
		Anagrafica anagraficaSaved = anagraficaService.insert(anagrafica);		
		return anagraficaSaved;
	}
		
	@PutMapping(_URL)//       "/rest/anagrafica"
	@ResponseBody
	public Anagrafica update(Anagrafica anagrafica){
		Anagrafica anagraficaSaved = anagraficaService.update(anagrafica);	
		return anagraficaSaved;
	}
	
	@DeleteMapping(_URL_PARAM)//       "/rest/anagrafica/{idAnagrafica}"
	@ResponseBody
	public void delete(@PathVariable("idAnagrafica") Integer idAnagrafica) {
		anagraficaService.delete(idAnagrafica);
	}	
}
