package com.betacom.utils;

import java.util.ArrayList;

import com.betacom.beans.Anagrafica;
import com.betacom.beans.Citta;
import com.betacom.beans.Province;
import com.betacom.beans.Stati;
import com.betacom.beans.Utenze;
import com.betacom.jpa.entities.AnagraficaEntity;
import com.betacom.jpa.entities.CittaEntity;
import com.betacom.jpa.entities.ProvinceEntity;
import com.betacom.jpa.entities.StatiEntity;
import com.betacom.jpa.entities.UtenzeEntity;

public class HRManagerUtils {
	public static Utenze convertToBean(UtenzeEntity utenzeEntity) {
		Utenze utenze = new Utenze(	utenzeEntity.getUsername(), 
												utenzeEntity.getPassword(),
												utenzeEntity.getAnagrafica().getIdAnagrafica(), 
												utenzeEntity.getAttivo(), 
												utenzeEntity.getDataScadenza(),
												utenzeEntity.getDataIns(),
												utenzeEntity.getUserIns(),
												utenzeEntity.getDataUpd(),																								
												utenzeEntity.getUserUpd(),
												utenzeEntity.getRuolo()
												);
		Anagrafica anagrafica = convertToBean(utenzeEntity.getAnagrafica());
		utenze.setAnagrafica(anagrafica);

		return utenze;
	}

	public static UtenzeEntity convertToEntity(Utenze utenze) {
		UtenzeEntity utenzeEntity = new UtenzeEntity(	utenze.getUsername(), 
														utenze.getPassword(),
														utenze.getIdAnagrafica(), 
														utenze.getAttivo(), 
														utenze.getDataScadenza(),
														utenze.getDataIns(),
														utenze.getUserIns(),
														utenze.getDataUpd(),																								
														utenze.getUserUpd(),
														utenze.getRuolo()
													);
		return utenzeEntity;
	}
	
	public static Anagrafica convertToBean(AnagraficaEntity anagraficaEntity) {
		Anagrafica anagrafica = new Anagrafica(	anagraficaEntity.getIdAnagrafica(), 
												anagraficaEntity.getCognome(),
												anagraficaEntity.getNome(), 
												anagraficaEntity.getDataNascita(), 
												anagraficaEntity.getSesso(),
												anagraficaEntity.getTelefono(), 
												anagraficaEntity.getMail(), 
												anagraficaEntity.getStatoCivile(),
												anagraficaEntity.getCodFiscale(), 
												anagraficaEntity.getIban(), 
												anagraficaEntity.getPrivacy(),
												anagraficaEntity.getIncensurato(), 
												anagraficaEntity.getDataIns(), 
												anagraficaEntity.getUserIns(),
												anagraficaEntity.getDataUpd(), 
												anagraficaEntity.getUserUpd()
												);
		
		return anagrafica;
	}

	public static AnagraficaEntity convertToEntity(Anagrafica anagrafica) {
		AnagraficaEntity anagraficaEntity = new AnagraficaEntity(anagrafica.getIdAnagrafica(), 
						anagrafica.getCognome(),
						anagrafica.getNome(), 
						anagrafica.getDataNascita(), 
						anagrafica.getSesso(),
						anagrafica.getTelefono(), 
						anagrafica.getMail(), 
						anagrafica.getStatoCivile(),
						anagrafica.getCodFiscale(), 
						anagrafica.getIban(), 
						anagrafica.getPrivacy(),
						anagrafica.getIncensurato(), 
						anagrafica.getDataIns(), 
						anagrafica.getUserIns(),
						anagrafica.getDataUpd(), 
						anagrafica.getUserUpd()
						);
		return anagraficaEntity;
	}
	
	public static StatiEntity convertToEntity(Stati stati) {
		StatiEntity statiEntity = new StatiEntity(stati.getIdStato(),stati.getDescrizione());
		return statiEntity;
	}
	public static Stati convertToBean(StatiEntity statiEntity) {
		return convertToBean(statiEntity,true);
	}
	
	public static Stati convertToBean(StatiEntity statiEntity,boolean getProvincia) {
		Stati stati = new Stati(statiEntity.getIdStato(),statiEntity.getDescrizione());
		
		if(getProvincia==true) {
			ArrayList<Province> listaProvince = new ArrayList<Province>();
			
			if(statiEntity.getListaProvince()!=null)
				for (ProvinceEntity provinciaEntity : statiEntity.getListaProvince()) {
					Province provincia = convertToBean(provinciaEntity,false);
					listaProvince.add(provincia);
				}
				
			stati.setListaProvince(listaProvince);
		}	
		return stati;
	}
	
	public static ProvinceEntity convertToEntity(Province province) {
		ProvinceEntity provinceEntity = new ProvinceEntity(province.getIdProvincia(),province.getDescrizione());
		if(province.getStato()!=null) {
			StatiEntity stati = convertToEntity(province.getStato());
			provinceEntity.setStato(stati);
		}
		return provinceEntity;
	}
	public static Province convertToBean(ProvinceEntity provinceEntity) {
		return convertToBean(provinceEntity,true);
	}
	public static Province convertToBean(ProvinceEntity provinceEntity,boolean getStato) {
		Province province = new Province(provinceEntity.getIdProvincia(),provinceEntity.getDescrizione());
		if(getStato==true) {
			Stati stato = convertToBean(provinceEntity.getStato(),false);
			province.setStato(stato);
			ArrayList<Citta> listaCitta= new ArrayList<Citta>();
			if(provinceEntity.getListaCitta()!=null)
				for (CittaEntity cittaEntity : provinceEntity.getListaCitta()) {
					Citta citta = convertToBean(cittaEntity,false);
					listaCitta.add(citta);
				}
			province.setListaCitta(listaCitta);
		}else if(provinceEntity.getStato()!=null){
			Stati stato = new Stati();
			stato.setIdStato(provinceEntity.getStato().getIdStato());
			province.setStato(stato);
		}
		return province;
	}
	
	public static CittaEntity convertToEntity(Citta citta) {
		CittaEntity cittaEntity = new CittaEntity(citta.getIdCitta(),citta.getDescrizione());
		ProvinceEntity province = convertToEntity(citta.getProvincia());
		cittaEntity.setProvincia(province);
		return cittaEntity;
	}
	public static Citta convertToBean(CittaEntity cittaEntity) {
		return convertToBean(cittaEntity,true);
	}
	public static Citta convertToBean(CittaEntity cittaEntity,boolean getProvincia) {
		Citta citta = new Citta(cittaEntity.getIdCitta(),cittaEntity.getDescrizione());
		if(getProvincia==true) {
			Province provincia = convertToBean(cittaEntity.getProvincia(),false);
			citta.setProvincia(provincia);
		}else {
			Province provincia = new Province();
			provincia.setIdProvincia(cittaEntity.getProvincia().getIdProvincia());
			citta.setProvincia(provincia);
		}
		return citta;
	}
}
