package com.betacom.configurations;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;
	
	@Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
        System.out.println(bCryptPasswordEncoder.encode("admin"));
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.
                authorizeRequests()
                
                .antMatchers("/login").permitAll()                
                .antMatchers("/rest/**").permitAll()
                
                .antMatchers("/homepage").hasAnyAuthority("ADMIN","USER")
                
                .antMatchers("/anagrafica").hasAnyAuthority("ADMIN","USER")
                .antMatchers("/anagrafica/**").hasAnyAuthority("ADMIN")
                
                
                .antMatchers("/utenze").hasAnyAuthority("ADMIN")
                .antMatchers("/utenze/**").hasAnyAuthority("ADMIN")
                
//                .antMatchers("/utenze").permitAll()
//                .antMatchers("/utenze/**").permitAll()
                
                .antMatchers("/stati").hasAnyAuthority("ADMIN")
                .antMatchers("/stati/**").hasAnyAuthority("ADMIN")
                
                .antMatchers("/province").hasAnyAuthority("ADMIN")
                .antMatchers("/province/**").hasAnyAuthority("ADMIN")
                
                .antMatchers("/citta").hasAnyAuthority("ADMIN")
                .antMatchers("/citta/**").hasAnyAuthority("ADMIN")

                
                .anyRequest()
                .authenticated().and().csrf().disable().formLogin()
                .loginPage("/login").failureUrl("/login?error=true")
                .defaultSuccessUrl("/homepage",true)
                .usernameParameter("username")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/").and().exceptionHandling()
                .accessDeniedPage("/access-denied");
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/static/**","/css/**","/js/**","/images/**");
    }
	
}
