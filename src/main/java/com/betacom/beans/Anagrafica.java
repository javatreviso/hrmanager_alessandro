package com.betacom.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Anagrafica {

	private Integer idAnagrafica;
	private String cognome;
	private String nome;
	private Date dataNascita;
	private String sesso;
	private String telefono;
	private String mail;
	private String statoCivile;
	private String codFiscale;
	private String iban;
	private Boolean privacy;
	private Boolean incensurato;
	private Date dataIns;
	private String userIns;
	private Date dataUpd;
	private String userUpd;

	public Anagrafica() {
		super();
	}

	public Anagrafica(Integer idAnagrafica, String cognome, String nome, Date dataNascita, String sesso,
			String telefono, String mail, String statoCivile, String codFiscale, String iban, Boolean privacy,
			Boolean incensurato, Date dataIns, String userIns, Date dataUpd, String userUpd) {
		super();
		this.idAnagrafica = idAnagrafica;
		this.cognome = cognome;
		this.nome = nome;
		this.dataNascita = dataNascita;
		this.sesso = sesso;
		this.telefono = telefono;
		this.mail = mail;
		this.statoCivile = statoCivile;
		this.codFiscale = codFiscale;
		this.iban = iban;
		this.privacy = privacy;
		this.incensurato = incensurato;
		this.dataIns = dataIns;
		this.userIns = userIns;
		this.dataUpd = dataUpd;
		this.userUpd = userUpd;
	}

	public Integer getIdAnagrafica() {
		return idAnagrafica;
	}

	public void setIdAnagrafica(Integer idAnagrafica) {
		this.idAnagrafica = idAnagrafica;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public String getDataNascitaString() {
		if (this.dataNascita != null) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
			return sf.format(this.dataNascita);
		} else
			return null;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public void setDataNascitaString(String dataNascita) {
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			this.dataNascita = sf.parse(dataNascita);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getStatoCivile() {
		return statoCivile;
	}

	public void setStatoCivile(String statoCivile) {
		this.statoCivile = statoCivile;
	}

	public String getCodFiscale() {
		return codFiscale;
	}

	public void setCodFiscale(String codFiscale) {
		this.codFiscale = codFiscale;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Boolean getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Boolean privacy) {
		this.privacy = privacy;
	}

	public Boolean getIncensurato() {
		return incensurato;
	}

	public void setIncensurato(Boolean incensurato) {
		this.incensurato = incensurato;
	}

	public Date getDataIns() {
		return dataIns;
	}

	public String getDataInsString() {
		if (dataIns != null) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sf.format(this.dataIns);
		} else
			return null;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public void setDataInsString(String dataIns) {
		if (dataIns != null && !dataIns.isEmpty()) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				this.dataIns = sf.parse(dataIns);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else
			this.dataIns=null;
	}

	public String getUserIns() {
		return userIns;
	}

	public void setUserIns(String userIns) {
		this.userIns = userIns;
	}

	public Date getDataUpd() {
		return dataUpd;
	}

	public String getDataUpdString() {
		if (dataUpd != null) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sf.format(this.dataUpd);
		} else
			return null;
	}

	public void setDataUpd(Date dataUpd) {
		this.dataUpd = dataUpd;
	}

	public void setDataUpdString(String dataUpd) {
		if (dataUpd != null && !dataUpd.isEmpty()) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				this.dataUpd = sf.parse(dataUpd);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else
			this.dataUpd = null;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}
}
