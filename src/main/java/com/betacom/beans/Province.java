package com.betacom.beans;

import java.util.ArrayList;

public class Province {
	
	private Integer idProvincia;
	private String 	descrizione;
	private Stati stato;
	
	private ArrayList<Citta> listaCitta;
	
	public Province() {
		super();
	}

	public Province(Integer idProvincia, String descrizione) {
		super();
		this.idProvincia = idProvincia;
		this.descrizione = descrizione;
	}
	
	public Integer getIdProvincia() {
		return idProvincia;
	}
	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Stati getStato() {
		return stato;
	}

	public void setStato(Stati stato) {
		this.stato = stato;
	}

	public ArrayList<Citta> getListaCitta() {
		return listaCitta;
	}

	public void setListaCitta(ArrayList<Citta> listaCitta) {
		this.listaCitta = listaCitta;
	}
	
}
