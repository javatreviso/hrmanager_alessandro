package com.betacom.beans;

import java.util.ArrayList;

public class Stati {
	
	private Integer idStato;
	private String 	descrizione;
	
	ArrayList<Province> listaProvince;
	
	public Stati() {
		super();
	}

	public Stati(Integer idStato, String descrizione) {
		super();
		this.idStato = idStato;
		this.descrizione = descrizione;
	}
	
	public Integer getIdStato() {
		return idStato;
	}
	public void setIdStato(Integer idStato) {
		this.idStato = idStato;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public ArrayList<Province> getListaProvince() {
		return listaProvince;
	}

	public void setListaProvince(ArrayList<Province> listaProvince) {
		this.listaProvince = listaProvince;
	}
}
