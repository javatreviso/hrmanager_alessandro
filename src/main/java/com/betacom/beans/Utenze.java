package com.betacom.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utenze {
	
	private String 	username;
	private String 	password;
	private Integer	idAnagrafica;
	private Boolean attivo;
	private	Date	dataScadenza;
	private Date 	dataIns;
	private String 	userIns;
	private Date 	dataUpd;
	private String 	userUpd;
	
	private String	ruolo;
	
	private Anagrafica anagrafica;
	
		
	public Utenze() {
		super();
	}

	public Utenze(String username, String password, Integer idAnagrafica, Boolean attivo, Date dataScadenza,
			Date dataIns, String userIns, Date dataUpd, String userUpd, String ruolo) {
		super();
		this.username = username;
		this.password = password;
		this.idAnagrafica = idAnagrafica;
		this.attivo = attivo;
		this.dataScadenza = dataScadenza;
		this.dataIns = dataIns;
		this.userIns = userIns;
		this.dataUpd = dataUpd;
		this.userUpd = userUpd;
		this.ruolo = ruolo;
	}



	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getIdAnagrafica() {
		return idAnagrafica;
	}
	public void setIdAnagrafica(Integer idAnagrafica) {
		this.idAnagrafica = idAnagrafica;
	}
	public Boolean getAttivo() {
		return attivo;
	}
	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}	
	public Date getDataScadenza() {
		return dataScadenza;
	}

	public void setDataScadenza(Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public String getDataScadenzaString() {
		if (this.dataScadenza != null) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
			return sf.format(this.dataScadenza);
		} else
			return null;
	}
	public void setDataScadenzaString(String dataScadenza) {
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			this.dataScadenza = sf.parse(dataScadenza);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	public Date getDataIns() {
		return dataIns;
	}

	public String getDataInsString() {
		if (dataIns != null) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sf.format(this.dataIns);
		} else
			return null;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public void setDataInsString(String dataIns) {
		if (dataIns != null && !dataIns.isEmpty()) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				this.dataIns = sf.parse(dataIns);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else
			this.dataIns=null;
	}

	public String getUserIns() {
		return userIns;
	}

	public void setUserIns(String userIns) {
		this.userIns = userIns;
	}

	public Date getDataUpd() {
		return dataUpd;
	}

	public String getDataUpdString() {
		if (dataUpd != null) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sf.format(this.dataUpd);
		} else
			return null;
	}

	public void setDataUpd(Date dataUpd) {
		this.dataUpd = dataUpd;
	}

	public void setDataUpdString(String dataUpd) {
		if (dataUpd != null && !dataUpd.isEmpty()) {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				this.dataUpd = sf.parse(dataUpd);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else
			this.dataUpd = null;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}

	public Anagrafica getAnagrafica() {
		return anagrafica;
	}

	public void setAnagrafica(Anagrafica anagrafica) {
		this.anagrafica = anagrafica;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	
}
