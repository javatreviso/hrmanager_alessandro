package com.betacom.beans;

public class Citta {
	
	private Integer idCitta;
	private String 	descrizione;
	private Province provincia;
	
	
	public Citta() {
		super();
	}

	public Citta(Integer idCitta, String descrizione) {
		super();
		this.idCitta = idCitta;
		this.descrizione = descrizione;
	}
	
	public Integer getIdCitta() {
		return idCitta;
	}
	public void setIdCitta(Integer idCitta) {
		this.idCitta = idCitta;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Province getProvincia() {
		return provincia;
	}

	public void setProvincia(Province provincia) {
		this.provincia = provincia;
	}
	
}
